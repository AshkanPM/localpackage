/**
 * @class Button
 */

import * as React from 'react'

const buttonClass = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '20px 40px',
    marginTop: '20px',
    fontSize: '16px',
    borderRadius: '3px',
    backgroundColor: 'cadetblue',
    color: 'white',
    textDecoration: 'none'
}

export type Props = { href: string; text: string }

export default class Button extends React.Component<Props> {
    render() {
        const { href, text } = this.props

        return (
            <a href={href} style={buttonClass}>
                {text}
            </a>
        )
    }
}
